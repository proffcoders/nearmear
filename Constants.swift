//
//  Constants.swift
//  NaerMeAR
//
//  Created by Roman Runner on 5/23/17.
//  Copyright © 2017 Roman Bigun LLC. All rights reserved.
//
import UIKit

let kNotificationFilterDidChange = "kNotificationFilterDidChange"

let locatorRadius                     = 500
let visiblePlacesCount                = 50
var currentFilter                     = "establishment" {
  willSet(newValue){
    NotificationCenter.default.post(name: Notification.Name(kNotificationFilterDidChange),
                                    object: newValue,
                                    userInfo:nil )
  }

}
let infoViewTopConstraintConstantHide = 1500.0
let infoViewTopConstraintConstantShow = 60.0

typealias EXTREMAL_EXECUTION = DispatchQoS.QoSClass
let queueUtility = DispatchQueue.global(qos:EXTREMAL_EXECUTION.utility)
let queueUserInitiated = DispatchQueue.global(qos:EXTREMAL_EXECUTION.userInitiated)
let queueUserInteractiive = DispatchQueue.global(qos:EXTREMAL_EXECUTION.userInteractive)
let queueBackground = DispatchQueue.global(qos:EXTREMAL_EXECUTION.background)

struct TimeToTarget {

  let metersPerMinute:Double = 5000/60
  var description:String
  var timeToGo:Double = 0.0 {
    willSet(oldValue){
      self.description = "\(Int(oldValue)) min"
    }
  }
  var distanceToTarget:Double = 0.0 {
    willSet(newValue) {
      self.timeToGo = newValue / metersPerMinute
    }
  }
  init() {
    self.distanceToTarget = 0.0
    self.timeToGo         = 0.0
    self.description      = " "
    self.distanceToTarget = 0.0
  
  }

}

extension String {
  
  func replaceDashesWithWSAndCapitalizeEachWord() -> String {
    
    let tmp = self.components(separatedBy: "_")
    var retVal = ""
    for item in tmp {
      retVal.append(item.capitalized)
      retVal.append(" ")
    }
    return retVal
    
  }
  
}
