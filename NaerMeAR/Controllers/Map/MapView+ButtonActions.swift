//
//  MapView+ButtonActions.swift
//  NaerMeAR
//
//  Created by Roman Runner on 5/27/17.
//  Copyright © 2017 Roman Bigun LLC. All rights reserved.
//

import UIKit


extension MapViewController {
  
 
  @IBAction func showARController(_ sender: Any) {
    arViewController                                      = ARViewController()
    arViewController.dataSource                           = self
    arViewController.maxDistance                          = 0
    arViewController.maxVisibleAnnotations                = visiblePlacesCount
    arViewController.maxVerticalLevel                     = 5
    arViewController.headingSmoothingFactor               = 0.05
    
    arViewController.trackingManager.userDistanceFilter   = 25
    arViewController.trackingManager.reloadDistanceFilter = 75
    arViewController.setAnnotations(self.places)
    arViewController.uiOptions.debugEnabled               = false
    arViewController.uiOptions.closeButtonEnabled         = true
    
    self.present(arViewController, animated: true, completion: nil)
    
    
  }

  @IBAction func displayFilterView(_ sender: UIButton) {
    displayFilter()
    
    
  }
}
