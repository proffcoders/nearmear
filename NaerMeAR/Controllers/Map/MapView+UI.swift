//
//  MapView+UI.swift
//  NaerMeAR
//
//  Created by Roman Runner on 5/27/17.
//  Copyright © 2017 Roman Bigun LLC. All rights reserved.
//

import UIKit

extension MapViewController {

  func customizeFilterView() {
    
    self.view.insertSubview(self.filterView, at:10 )
    self.filterView.translatesAutoresizingMaskIntoConstraints = false
    self.filterView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    self.filterView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 1).isActive = true
    self.filterView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.45).isActive = true
    
    self.filterViewRightConstraint = NSLayoutConstraint.init(item: self.filterView,
                                                             attribute: .right,
                                                             relatedBy: .equal,
                                                             toItem: self.view,
                                                             attribute: .right,
                                                             multiplier: 1,
                                                             constant: 250)
    self.filterViewRightConstraint.isActive = true
    
  }
  func displayFilter() {
    
    DispatchQueue.main.async {
      
      if self.filterViewRightConstraint.constant == -3 {
        
        self.filterViewRightConstraint.constant = 250
        
      } else {
        
        self.filterViewRightConstraint.constant = -3
        
      }
      UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
        
        self.view.layoutIfNeeded()
        
        
        
      })
      
      
    }
    
    
  }


}
