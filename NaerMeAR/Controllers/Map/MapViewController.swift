
import UIKit

import CoreLocation
import MapKit
import PulsingHalo
import ReachabilitySwift

enum InfoBoxDisplayTarget {

  case map
  case ar

}

class MapViewController: UIViewController,MKMapViewDelegate {
  
  var places                            = [Place]()
  fileprivate var heading: Double       = 0
  fileprivate var interactionInProgress = false
  var filterName:String = "" {
    willSet(oldValue) {
        self.navigationItem.title = oldValue
    
    }
  }
  fileprivate lazy var locationManager:CLLocationManager = {
    
    var _locationManager = CLLocationManager()
    _locationManager.delegate = self
    _locationManager.distanceFilter = 10
    _locationManager.activityType = .fitness
    _locationManager.allowsBackgroundLocationUpdates = true
    _locationManager.pausesLocationUpdatesAutomatically = true
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest
    _locationManager.startUpdatingLocation()
    _locationManager.requestWhenInUseAuthorization()
    
    return _locationManager
  }()
  
  var startedLoadingPOIs                = false
  var photoStack:Array<String>          = []
  var reviews:Array<Review>             = []
  var arViewController: ARViewController!
  var filterViewRightConstraint: NSLayoutConstraint!
  var userLocation:UserLocation?
  var objectData:NSDictionary?
  var placeObject:Place?
  var halo:PulsingHaloLayer!
  let internetAlert:CUMessageBox = CUMessageBox()
  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var infoView: InfoView!
  @IBOutlet weak var infoViewMap: InfoView!
  @IBOutlet weak var filterView:FilterView!
  var loader = PlacesLoader()
  
  deinit {
    NotificationCenter.default.removeObserver(self,
                                              name: NSNotification.Name(rawValue: kNMUpdateAnnotationsNotification),
                                              object: nil)
  }
  func loadHalo() {
   
      if  halo == nil {
          halo                 = PulsingHaloLayer()
          halo.position        = view.center
          halo.haloLayerNumber = 5
          halo.radius          = CGFloat(locatorRadius)
          view.layer.addSublayer(halo)
          halo.start()
    }
  }
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    mapView.delegate = self
    addNotificationObserver()
    internetAlert.initWithPlaceholder(controller: self)
    mapView.userTrackingMode = MKUserTrackingMode.followWithHeading
    customizeFilterView()
    NotificationCenter.default.addObserver(self, selector: #selector(useFilter), name: NSNotification.Name(rawValue: kNMUpdateAnnotationsNotification), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(filterChanged), name: NSNotification.Name(rawValue: kNotificationFilterDidChange), object: nil)
    
  }
  func filterChanged(notification:Notification) {
  
    self.filterName = notification.object as! String
  
  }
  
  override func viewDidAppear(_ animated: Bool) {
    locationManager.startUpdatingHeading()
    loadHalo()
  }
  override func viewDidDisappear(_ animated: Bool) {
    locationManager.stopUpdatingHeading()
    
  }
  
  func useFilter(_ notification:Notification) {
    displayFilter()
    self.locationManager.startUpdatingLocation()
    
  }
  

  
  func getMoreDetailsFor(annotation:Place) {
    
    let placesLoader = PlacesLoader()
    
    placesLoader.loadDetailInformation(forPlace: annotation) { resultDict, error,requestStatus in
      
      if let infoDict         = resultDict?.object(forKey: "result") as? NSDictionary {
        
          annotation.phoneNumber = infoDict.object(forKey: "formatted_phone_number") as? String
          annotation.website     = infoDict.object(forKey: "website") as? String
          let workingPlan        = infoDict.object(forKey: "opening_hours") as? NSDictionary
        
        if let wd = workingPlan?["weekday_text"] as? [String] {
            var result = ""
            for day in wd {
                result.append(day+"\n")
            }
          annotation.workingHoursByDays = result
          
          if let opened = workingPlan?["open_now"] {
            annotation.opennow = opened as? Bool
          }
        }
        
        //MARK: Parse reviews and insert object to current Place for future work with
        if annotation.reviews.count==0 {
          
          self.reviews = []
          if infoDict.object(forKey: "reviews") != nil {
            
            if let rev = infoDict.object(forKey: "reviews") as? Array<Dictionary<String,Any>> {
              
              for item in rev {
                
                var photoUrl = ""
                
                if item["profile_photo_url"] != nil {
                  photoUrl = item["profile_photo_url"] as! String
                  
                }
                
                let revItem = Review(authorAvatar: photoUrl,
                                     message: item["text"]! as! String,
                                     userRating: item["rating"] as! Double,
                                     authorName:item["author_name"]! as! String)
                
                self.reviews.append(revItem)
                
              }
            }
            annotation.reviews = self.reviews
          }
          
        }
        
        self.photoStack = []
        
        if infoDict.object(forKey: "photos") != nil {
          let photos = infoDict.object(forKey: "photos") as! Array<Any>
          
          for photo in photos  {
            
            let d = photo as! NSDictionary
            let photoReference = d["photo_reference"] as! String
            self.photoStack.append(photoReference)
            
          }
          
          if self.photoStack.count > 0 {
            annotation.photoStack = self.photoStack
            
          }
        }
        
        let sd = CLLocation(latitude:(self.userLocation?.latitude)!, longitude: (self.userLocation?.longitude)!)
        let distanceToTarget = Double((annotation.location?.distance(from: sd))!)
        annotation.distanceFromUser = distanceToTarget
        
          self.placeObject = annotation
          self.objectData = infoDict
          
            DispatchQueue.main.async {
              self.showDetails()
              
            }

        
        
      }
    }
    
  }
  func showDetails() {
  
    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let webController: PlaceInfoTableViewController = storyboard.instantiateViewController(withIdentifier: "PlaceInfoTableViewController") as! PlaceInfoTableViewController
    webController.dataSource = self.placeObject
    webController.userLocation = self.userLocation
    self.navigationController?.pushViewController(webController, animated: true)
  
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  func clearRouting() {
  
    let overlays = mapView.overlays
    mapView.removeOverlays(overlays)
  
  }
  
}

extension MapViewController: CLLocationManagerDelegate {
  func locationManagerShouldDisplayHeadingCalibration(_ manager: CLLocationManager) -> Bool {
    return true
  }
  
  func createAnnotationWith(placesArray:[NSDictionary]) {
    
    for placeDict in placesArray {
      
      let place    = Place(placeDict:placeDict)
      self.places.append(place)
      let annotation = PlaceAnnotation(location: place.location!.coordinate, title: place.placeName,placeInfo:place)
      
      DispatchQueue.main.async {
        self.mapView.addAnnotation(annotation)
        
      }

    }
    
  }
 
  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    
    
    let pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "myPin")
    
    pinAnnotationView.pinTintColor = .purple
    pinAnnotationView.isDraggable = true
    pinAnnotationView.canShowCallout = true
    pinAnnotationView.animatesDrop = true
    self.mapView.showsUserLocation = true
    let infoButton = UIButton.init(type: UIButtonType.infoLight) as UIButton
    
    infoButton.frame.size.width = 44
    infoButton.frame.size.height = 44
    infoButton.backgroundColor = UIColor.clear
    
    if let a = annotation as? PlaceAnnotation {
      
      let placeImage = UIImageView(frame: CGRect(x: 1, y: 1, width: 42, height: 42))
      placeImage.imageFromUrl(urlString: a.place.imageAbsolutePath, spinner: nil, rounded: true)
      placeImage.backgroundColor = UIColor.clear
      pinAnnotationView.leftCalloutAccessoryView = placeImage
      pinAnnotationView.rightCalloutAccessoryView = infoButton
      placeImage.layer.cornerRadius = 6
      placeImage.layer.borderColor = UIColor.black.cgColor
      placeImage.layer.borderWidth = 1
      
    }
    
    return pinAnnotationView
    
    
  }
  func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
    
    if let a = view.annotation as? PlaceAnnotation {
        getMoreDetailsFor(annotation: a.place)
    }
   
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
    DispatchQueue.main.async {
      
      self.loadHalo()
    }
    
    if locations.count > 0 {
      
      self.places = []
      self.mapView.removeAnnotations(self.mapView.annotations)
      let location = locations.last!
      print("Speed: \(location.speed)")
      
      if location.horizontalAccuracy < 100 {
        
        manager.stopUpdatingLocation()
        
        let span = MKCoordinateSpan(latitudeDelta: 0.014, longitudeDelta: 0.014)
        let region = MKCoordinateRegion(center: location.coordinate, span: span)
        mapView.region = region
        
        self.userLocation = UserLocation(longitude: location.coordinate.longitude, latitude:location.coordinate.latitude)
        if !startedLoadingPOIs {
          
          startedLoadingPOIs = true
          
          loader.loadPOIS(location: location, radius: locatorRadius) { placesDict, error,requestStatus in
            
            if let dict = placesDict {
              
              guard let placesArray = dict.object(forKey: "results") as? [NSDictionary]  else { return }
              
              self.createAnnotationWith(placesArray: placesArray)
              
              self.startedLoadingPOIs = false
              
              DispatchQueue.main.async {
                self.halo.stop()
               }
              
            } else {
            
              self.startedLoadingPOIs = false
              DispatchQueue.main.async {
                self.halo.stop()
              }
              
              
            }
            
          }
        }
      }
    }
  }
 }

extension MapViewController: ARDataSource {
  
  func ar(_ arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
    
    let annotationView        = AnnotationView()
    annotationView.annotation = viewForAnnotation
    annotationView.delegate   = self
    annotationView.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
    
    return annotationView
  }
}

extension MapViewController: AnnotationViewDelegate {
  
  func didTouch(annotationView: AnnotationView) {
    
    if let annotation = annotationView.annotation as? Place {
      
      getMoreDetailsFor(annotation:annotation)
      
    }
  }
}
extension PulsingHaloLayer {

  func stop() {
    self.removeFromSuperlayer()
    self.sublayers = []
    
  }

}
extension MapViewController : InternetReachabilityProtocol {

  func addNotificationObserver() {
    
    NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(_:)), name: notificationName, object:nil)
    
  }
  func reachabilityChanged(_ note: Notification) {
    
    let reachability = note.object as! Reachability
    
    if reachability.isReachable {
      
      internetAlert.displayWith(state: false)
      
      switch(requestWorker.requestType) {
          case .detailedRequest: break
          case .mainRequest:
            self.locationManager.startUpdatingLocation()
          }
      
      
    } else {
      internetAlert.displayWith(state: true)
    }
  }


}
