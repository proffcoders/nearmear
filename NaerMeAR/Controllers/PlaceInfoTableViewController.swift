//
//  PlaceInfoTableViewController.swift
//  NaerMeAR
//
//  Created by Roman Runner on 6/1/17.
//  Copyright © 2017 Roman Bigun LLC. All rights reserved.
//

import UIKit

class PlaceInfoTableViewController: UITableViewController {

  @IBOutlet weak var reviewLbl: UILabel!
  @IBOutlet weak var callButton: UIButton!
  @IBOutlet weak var wwwButton: UIButton!
  @IBOutlet weak var nameLbl: UILabel!
  @IBOutlet weak var infoLbl: UILabel!
  @IBOutlet weak var image: UIImageView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  var dataSource:Place?
  var userLocation:UserLocation?
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
      if let placeName = self.dataSource?.placeName {
        self.navigationItem.title = placeName
      }
      

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }

    @IBAction func back(_ sender:UIButton) {
      
      self.navigationController?.popViewController(animated: true)
      
    }
  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
      
      switch indexPath.row {
      
          case 0:
              let cell = tableView.dequeueReusableCell(withIdentifier: "InfoImagesTableViewCell", for: indexPath) as! InfoImagesTableViewCell
              cell.place = self.dataSource
              return cell
          
          case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MainInfoCell", for: indexPath) as! MainInfoTableViewCell
            cell.showInfoFor(place: dataSource!)
            return cell
            
          case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoReviewLine", for: indexPath) as! ReviewInfoTableViewCell
            cell.initWith(reviewsCount: (self.dataSource?.reviews.count)!, rating:self.dataSource!.rating)
              return cell
          case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewsTableViewCell", for: indexPath) as! ReviewsTableViewCell
            cell.place = self.dataSource
            return cell
          case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MapTableViewCell", for: indexPath) as! MapTableViewCell
            
            cell.initCellWith(place: self.dataSource!, userLocation: self.userLocation!)
            
            return cell
        
          default : print("No cells")
        

      }
      
      
      return cell!
            
  }
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
      return indexPath.heightForCurrentIndexPath()
  }
  @IBAction func callButtonPress(_ sender: UIButton) {
    
    let telChars = (self.self.dataSource?.phoneNumber ?? "").characters.filter({$0 != " " })
    let tel = telChars.reduce("") { $0 + String($1) }
    let url = URL(string: "tel:\(tel)")
    
    
    if let phoneCallURL:URL = url
    {
      let application:UIApplication = UIApplication.shared
      if (application.canOpenURL(phoneCallURL)) {
        let alertController = UIAlertController(title: "NearMeAR",
                                                message: "Are you sure you want to call \n\(self.self.dataSource?.placeName ?? "")?", preferredStyle: .alert)
        let yesPressed = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
          application.openURL(phoneCallURL)
        })
        let noPressed = UIAlertAction(title: "No", style: .default, handler: { (action) in
          
        })
        alertController.addAction(yesPressed)
        alertController.addAction(noPressed)
        
        self.present(alertController, animated: true, completion: nil)
        
      }
    }
    
  }
  @IBAction func webButtonPress(_ sender: UIButton) {
    
    let storyboard: UIStoryboard         = UIStoryboard(name: "Main", bundle: nil)
    let webController: WebViewController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
    webController.webLink                = self.dataSource?.website
    
    self.present(webController, animated: true, completion: nil)
  
    
  }
  
  

}

extension IndexPath {

  func heightForCurrentIndexPath() -> CGFloat {
  
    if self.row == 0 { return 181 }
    if self.row == 1 { return 469 }
    if self.row == 2 { return 36  }
    if self.row == 3 { return 198 }
    if self.row == 4 { return 357 }
    
    return 0
    
  
  }

}
