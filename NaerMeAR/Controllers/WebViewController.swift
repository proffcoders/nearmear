//
//  WebViewController.swift
//  NearMeAR
//
//  Created by Roman Runner on 5/21/17.
//  Copyright © 2017 Roman Bigun LLC All rights reserved.
//

import UIKit

class WebViewController: UIViewController,UIWebViewDelegate {
  
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var webView: UIWebView!
    var webLink:String?
    override func viewDidLoad() {
        super.viewDidLoad()
      webView.delegate = self
      webView.loadRequest(URLRequest(url: URL(string:webLink!)!))
      
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   @IBAction func dismissButtonPress(_ sender: UIButton) {
   
       self.dismiss(animated: false, completion: nil)
    
   }
  //MARK: WebView delegates methods
  func webViewDidStartLoad(_ webView: UIWebView) {
    activityIndicator.isHidden = false
    activityIndicator.startAnimating()
  }
  func webViewDidFinishLoad(_ webView: UIWebView){
    activityIndicator.stopAnimating()
    activityIndicator.isHidden = true
  }
  func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
    activityIndicator.stopAnimating()
    activityIndicator.isHidden = true
  }

}
