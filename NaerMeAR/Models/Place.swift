
import Foundation
import CoreLocation

class Place: ARAnnotation {
  
  let reference: String
  let type:String?
  let photoReference: String
  let placeName: String
  let address: String
  var phoneNumber: String?
  var website: String?
  var iconPath: String
  var imageAbsolutePath: String
  var rating:Double
  var reviews    = [Review]()
  var photoStack = [String]()
  var workingHoursByDays:String?
  var opennow:Bool?
  var infoText: String {
    get {
      var info = "Address: \(address)"
      
      if phoneNumber != nil {
        info += "\nPhone: \(phoneNumber!)"
      }
      
      if website != nil {
        info += "\nWeb: \(website!)"
      }
      if type != nil {
        info += "\nType: \(type!)"
      }
      
      return info
    }
  }
  convenience init(placeDict:NSDictionary) {
    
    let latitude  = placeDict.value(forKeyPath: "geometry.location.lat") as! CLLocationDegrees
    let longitude = placeDict.value(forKeyPath: "geometry.location.lng") as! CLLocationDegrees
    let reference = placeDict.object(forKey: "reference") as! String
    let name      = placeDict.object(forKey: "name") as! String
    let address   = placeDict.object(forKey: "vicinity") as! String
    let iconPath  = placeDict.object(forKey: "icon") as! String
    let types     = placeDict.value(forKey: "types") as! Array<String>
    let type      = types[0]
    var photoReference = ""
    
    
    if placeDict.object(forKey: "photos") != nil {
      let photos = placeDict.object(forKey: "photos") as! Array<Any>
      
      let d = photos[0] as! NSDictionary
      photoReference = d["photo_reference"] as! String
      
      
    }
    let rating   = placeDict.object(forKey: "rating") as? Double ?? 0.0
    let location = CLLocation(latitude: latitude, longitude: longitude)
    
    self.init(location: location, reference: reference, name: name, address: address,photoReference:photoReference, icon:iconPath,type:type,rating:rating)
    
  }
  init(location: CLLocation, reference: String, name: String, address: String,photoReference:String, icon:String,type:String,rating:Double) {
    placeName              = name
    self.reference         = reference
    self.address           = address
    self.photoReference    = photoReference
    self.iconPath          = icon
    self.type              = type
    self.rating            = rating
    self.imageAbsolutePath = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=\(self.photoReference)&key=\(apiKey)"
    super.init()
    
    self.location = location
  }
  
  override var description: String {
    return placeName
  }
}
