//
//  Review.swift
//  NaerMeAR
//
//  Created by Roman Runner on 5/21/17.
//  Copyright © 2017 Roman Bigun LLC. All rights reserved.
//

import Foundation

struct Review {

  let authorAvatar:String
  let message:String
  let userRating:Double
  let authorName:String

}
