//
//  ImageView+Alamofire.swift
//  NearMeAR
//
//  Created by Roman Runner on 5/20/17.
//  Copyright © 2017 Roman Bigun LLC All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage

class RBCacher: NSCache<AnyObject, AnyObject> {
  
  static let shared:RBCacher = RBCacher()
  
}

extension UIImageView {
  
  
  public func imageFromUrl(urlString: String,spinner:UIActivityIndicatorView?, rounded:Bool=true) {
    
   
    if rounded {
      self.layer.cornerRadius = 6
      self.layer.borderColor = UIColor.black.cgColor
      self.layer.borderWidth = 1
    }
    if RBCacher.shared.object(forKey:urlString as AnyObject) != nil {
      self.image = RBCacher.shared.object(forKey:urlString as AnyObject) as? UIImage
      
    } else {
      
      var internalSpinner:UIActivityIndicatorView!
      if let spin = spinner {
        internalSpinner = spin
        internalSpinner.isHidden = false
        internalSpinner.startAnimating()
      }
      
      Alamofire.request(urlString).responseImage { response in
        if let image = response.result.value {
          DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3,  delay: 0, options: .curveLinear, animations: {
                  self.image = image
              
              
                  RBCacher.shared.setObject(self.image!, forKey: urlString as AnyObject)
                  if (internalSpinner != nil) {
                    internalSpinner.isHidden = true
                    internalSpinner.stopAnimating()
                  }
                  
                })//Animation ended
            }//Main queue end working
           }
          }
         }
        }
  
}
