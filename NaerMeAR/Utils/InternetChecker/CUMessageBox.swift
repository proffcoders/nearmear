//
//  CUMessageBox.swift
//  Tethers
//
//  Created by Roman Runner on 5/4/17.
//  Copyright © 2017 Roman Bigun. All rights reserved.
//
/* 
 USAGE:
 
 
 0. In AppDelegate add the foloowing string: 
 
 InternetConnectionChecker.sharedConnection.startObserveInternetConnection()
 
 1.Add property of CUMessageBox to the target controller like
 
 let internetAlert:CUMessageBox = CUMessageBox()
 
 Dont forget to add to the top of the file :
 import ReachabilitySwift
 
 2.Install CUMessageBox into (viewDidLoad method) target view controller like
 
 internetAlert.initWithPlaceholder(controller: self)
 
 3. Create extention which conforms to InternetReachabilityProtocol protocol
 
 extension YourViewController:InternetReachabilityProtocol {
 
   func addNotificationObserver() {
 
       NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(_:)), name: notificationName, object:nil)
 
    }
   func reachabilityChanged(_ note: Notification) {
 
      let reachability = note.object as! Reachability
 
      if reachability.isReachable {
         internetAlert.displayWith(state: false)
 
      } else {
         internetAlert.displayWith(state: true)
  }
 }
 
 }
 */
import UIKit


class CUMessageBox: UIView {
   
    enum CUConstrainsConstants:CGFloat {
        case toShow = 100
        case toHide = -1000
    }
    enum CUInternetConnectionStatus:String {
        case lost = "Internet connection lost!"
        case alive = "Internet connection back!"
    }
    enum CUMessageType {
    
        case typeInetrenConnection
        case typeCustom
    
    }
   private let CUFrameOffset:CGFloat = 20
   private var topConstraint: NSLayoutConstraint!
   private var parentController:UIViewController!
  
  
   lazy private var infoLbl:UILabel = {
    
        [unowned self] in
        var tmpLbl:UILabel = UILabel()
        tmpLbl.frame = CGRect(x: 10, y: self.frame.size.height/2+10, width: self.frame.size.width-5, height: 21)
        tmpLbl.font = UIFont.boldSystemFont(ofSize: 22)
        tmpLbl.textAlignment = .center
        return tmpLbl
    
    }()

    open func initWithPlaceholder(controller:UIViewController!){
        
        if let tmpController = controller {
            self.parentController = tmpController
            
            customizeView()
            customizeViewConstraints()
            addAndCustomizeLabel()
            
        }
    }
    open func displayWith(state:Bool) {
        
        DispatchQueue.main.async {
            if state == true {
                self.topConstraint.constant = CUConstrainsConstants.toShow.rawValue
                self.infoLbl.text = CUInternetConnectionStatus.lost.rawValue
                self.infoLbl.textColor = UIColor.white
                self.backgroundColor = UIColor.red
                UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                    self.parentController.view.layoutIfNeeded()
                    
                })
            } else {
                self.topConstraint.constant = CUConstrainsConstants.toHide.rawValue
                self.infoLbl.text = CUInternetConnectionStatus.alive.rawValue
                self.infoLbl.textColor = UIColor.white
                self.backgroundColor = UIColor.green
                UIView.animate(withDuration: 0.5, delay: 2, options: .curveEaseInOut, animations: {
                    self.parentController.view.layoutIfNeeded()
                    
                })
            }
            
            
            
        }
    }
    fileprivate func customizeView() {
        
        self.backgroundColor = UIColor.white
        self.layer.borderColor = UIColor.brown.cgColor
        self.layer.shadowRadius = 0.6
        self.layer.shadowColor = UIColor.brown.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 6
        self.frame = CGRect(x: CUFrameOffset,
                            y: CUFrameOffset,
                            width: self.parentController.view.frame.width-CUFrameOffset,
                            height: 200)
    
    }
    fileprivate func addAndCustomizeLabel() {
        self.addSubview(infoLbl)
        
    }
    fileprivate func customizeViewConstraints() {
        self.parentController.view.insertSubview(self, at:10 )
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.centerXAnchor.constraint(equalTo: self.parentController.view.centerXAnchor).isActive = true
        self.heightAnchor.constraint(equalTo: self.parentController.view.heightAnchor, multiplier: 0.35).isActive = true
        self.widthAnchor.constraint(equalTo: self.parentController.view.widthAnchor, multiplier: 1).isActive = true
        self.topConstraint = NSLayoutConstraint.init(item: self,
                                                     attribute: .top,
                                                     relatedBy: .equal,
                                                     toItem: self.parentController.view,
                                                     attribute: .top,
                                                     multiplier: 1,
                                                     constant: CUConstrainsConstants.toHide.rawValue)
        self.topConstraint.isActive = true
        
    }

}
