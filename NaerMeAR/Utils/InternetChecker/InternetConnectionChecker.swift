//
//  InternetConnectionChecker.swift
//  Tethers
//
//  Created by Roman Runner on 5/3/17.
//  Copyright © 2017 Roman Bigun. All rights reserved.
//

import Foundation
import SystemConfiguration
import ReachabilitySwift

let notificationName = Notification.Name("InternetConnectionStaus")
protocol InternetReachabilityProtocol {

    func addNotificationObserver()

}

enum ConnectionStatus:String {

    case Reachable = "InternetConnectionStausReachable"
    case Unreachable = "InternetConnectionStausUnreachable"
    case Default = "InternetConnectionStausDefault"
}
class InternetConnectionChecker {
    
    static var sharedConnection:InternetConnectionChecker = InternetConnectionChecker()
    var reachability = Reachability()!
    var isReachable:Bool = false {
    
        willSet {
           
            if newValue == true {
            
                NotificationCenter.default.post(name:notificationName,object: reachability)

            } else {
            
                NotificationCenter.default.post(name:notificationName,object: reachability)
            }
        }
    
    }
    private init() {
    
        
    
    }
    open func startObserveInternetConnection() {
        
        setupReachability(nil, useClosures: true)
        startNotifier()
        
    
    }
    
    func setupReachability(_ hostName: String?, useClosures: Bool) {
        
        
        let reachability = hostName == nil ? Reachability() : Reachability(hostname: hostName!)
        self.reachability = reachability!
        
        if useClosures {
            reachability?.whenReachable = { reachability in
                DispatchQueue.main.async {
                    self.isReachable = true
                    self.updateLabelColourWhenReachable(reachability)
                }
            }
            reachability?.whenUnreachable = { reachability in
                DispatchQueue.main.async {
                    self.isReachable = false
                    self.updateLabelColourWhenNotReachable(reachability)
                }
            }
        } else {
            NotificationCenter.default.addObserver(self, selector: #selector(InternetConnectionChecker.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        }
    }
    
    func startNotifier() {
        print("--- start notifier")
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start\nnotifier")
            return
        }
    }
    
    func stopNotifier() {
        print("--- stop notifier")
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: ReachabilityChangedNotification, object: nil)
        
    }
    
    func updateLabelColourWhenReachable(_ reachability: Reachability) {
        print("\(reachability.description) - \(reachability.currentReachabilityString)")
        if reachability.isReachableViaWiFi {
            print("WiFi is in using")
        } else {
            print("Celullar is in using")
        }
        
        print(reachability.currentReachabilityString)
    }
    
    func updateLabelColourWhenNotReachable(_ reachability: Reachability) {
        print("\(reachability.description) - \(reachability.currentReachabilityString)")
        print(reachability.currentReachabilityString)
    }
    
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            self.isReachable = true
            updateLabelColourWhenReachable(reachability)
        } else {
            self.isReachable = false
            updateLabelColourWhenNotReachable(reachability)
        }
    }
    
    deinit {
        stopNotifier()
    }
    
}
