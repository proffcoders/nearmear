
import Foundation
import CoreLocation

enum PLRequestState:String {
  case requestStartPerforming
  case requestLoaded
  case requestFailed
  case requestStateDefault
}
enum PLRequestType {

  case mainRequest
  case detailedRequest

}
struct PLRequest {
  var requestState:PLRequestState = .requestStateDefault
  var requestType:PLRequestType = .mainRequest
  var requestLastUrl:String = ""
}

let apiKey = "AIzaSyDVKb5CzSk_sBqon6NJ8WxGnfnQHZ5znQg"

var requestWorker:PLRequest = PLRequest(requestState: .requestStateDefault, requestType: .mainRequest,requestLastUrl: "")

struct PlacesLoader {
  
  fileprivate let apiURL = "https://maps.googleapis.com/maps/api/place/"
  
  var userLocation:UserLocation?
  
  
  mutating func loadPOIS(location: CLLocation, radius: Int = 30, handler: @escaping (NSDictionary?, NSError?,PLRequest) -> Void) {
    
    let (lat,lng) = (location.coordinate.latitude,location.coordinate.longitude)
    self.userLocation = UserLocation(longitude: lat, latitude: lng)
    
      requestWorker.requestState = .requestStartPerforming
      requestWorker.requestType = .mainRequest
    
    print("Load pois")
    
    let uri = apiURL + "nearbysearch/json?location=\(lat),\(lng)&radius=\(radius)&sensor=true&types=\(currentFilter)&key=\(apiKey)"
    
    requestWorker.requestLastUrl = uri
    
    if RBCacher.shared.object(forKey:uri as AnyObject) != nil {
      
      requestWorker.requestState = .requestLoaded
      handler((RBCacher.shared.object(forKey:uri as AnyObject) as? NSDictionary),nil,requestWorker)
      
      
      
    } else {
    
    
    let url = URL(string: uri)!
    let session = URLSession(configuration: URLSessionConfiguration.default)
    let dataTask = session.dataTask(with: url) { data, response, error in
      if let error = error {
        requestWorker.requestState = .requestFailed
        handler(nil, error as NSError,requestWorker)
        
      } else if let httpResponse = response as? HTTPURLResponse {
        if httpResponse.statusCode == 200 {
          requestWorker.requestState = .requestLoaded
          do {
            let responseObject = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
            guard let responseDict = responseObject as? NSDictionary else {
              return
            }
            RBCacher.shared.setObject(responseDict, forKey: uri as AnyObject)
            handler(responseDict, nil,requestWorker)
            
          } catch let error as NSError {
            requestWorker.requestState = .requestFailed
            handler(nil, error,requestWorker)
            
          }
        }
      }
    }
    
    dataTask.resume()
    }
  }
  
  func loadDetailInformation(forPlace: Place, handler: @escaping (NSDictionary?, NSError?,PLRequest) -> Void) {
    
    
    requestWorker.requestState = .requestStartPerforming
    requestWorker.requestType = .detailedRequest
    
    
    let uri = apiURL + "details/json?reference=\(forPlace.reference)&sensor=true&key=\(apiKey)"
    
    let url = URL(string: uri)!
    
    if RBCacher.shared.object(forKey:uri as AnyObject) != nil {
      requestWorker.requestState = .requestLoaded
      handler((RBCacher.shared.object(forKey:uri as AnyObject) as? NSDictionary),nil,requestWorker)
      
    } else {
    
    let session = URLSession(configuration: URLSessionConfiguration.default)
    let dataTask = session.dataTask(with: url) { data, response, error in
      if let error = error {
        
        print(error)
        requestWorker.requestState = .requestFailed
        handler(nil, nil,requestWorker)
      }
      else if let httpResponse = response as? HTTPURLResponse {
        if httpResponse.statusCode == 200 {
         requestWorker.requestState = .requestLoaded
          
          do {
            let responseObject = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
            guard let responseDict = responseObject as? NSDictionary else {
              return
            }
            RBCacher.shared.setObject(responseDict, forKey: uri as AnyObject)
            handler(responseDict, nil,requestWorker)
            
          } catch let error as NSError {
            requestWorker.requestState = .requestFailed
            handler(nil, error,requestWorker)
            
          }
        }
      }
    }
    
    dataTask.resume()
    }
  }
}
