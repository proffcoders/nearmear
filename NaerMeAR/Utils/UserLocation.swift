//
//  UserLocation.swift
//  NearMeAR
//
//  Created by Roman Runner on 5/20/17.
//  Copyright © 2017 Roman Bigun LLC All rights reserved.
//

import Foundation
import MapKit

struct UserLocation {
  
  let longitude:Double?
  let latitude:Double?

  var location:CLLocationCoordinate2D {
    
    get {
      let _location = CLLocationCoordinate2D(latitude: self.latitude!, longitude: self.longitude!)
      return _location
    }
    
  }
  

}
