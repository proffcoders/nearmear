
import UIKit

protocol AnnotationViewDelegate {
  func didTouch(annotationView: AnnotationView)
}


class AnnotationView: ARAnnotationView {
  var titleLabel: UILabel?
  var distanceLabel: UILabel?
  var delegate: AnnotationViewDelegate?
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    
    loadUI()
  }
  fileprivate func customizeAnotationView() {
  
    self.layer.cornerRadius = 6
    self.layer.borderWidth  = 2
    self.layer.borderColor  = UIColor.white.cgColor
    self.backgroundColor = UIColor(white: 0.3, alpha: 0.9)
    
  }
  func makeTitle() -> UILabel {
  
    let label           = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width-10, height: 30))
    label.font          = UIFont.systemFont(ofSize: 13)
    label.numberOfLines = 0
    label.textColor     = UIColor.white
    label.layer.cornerRadius = 6
    self.addSubview(label)
    
    return label
  
  }
  func makeDistanceLabel() {
  
    distanceLabel                = UILabel(frame: CGRect(x: 0, y: 27, width: self.frame.size.width-10, height: 20))
    distanceLabel?.textAlignment = .center
    distanceLabel?.font          = UIFont.systemFont(ofSize: 12)
    distanceLabel?.textColor = UIColor.white
    self.addSubview(distanceLabel!)
    
  }
  func createView() {
    // Type of venue icon. For e,g bed for hostels
    let icon = UIImageView(frame: CGRect(x: 0, y: 0, width: (distanceLabel?.frame.size.height)!+5, height: (distanceLabel?.frame.size.height)!))
    distanceLabel?.addSubview(icon)
    
    if let annotation = annotation as? Place {
      titleLabel?.text = " "+annotation.placeName
      distanceLabel?.text = annotation.type
      guard annotation.iconPath.characters.count > 0 else { return }
  
      icon.imageFromUrl(urlString: annotation.iconPath, spinner: nil, rounded: false)
      
    }
    
  }
  func loadUI() {
    
    titleLabel?.removeFromSuperview()
    distanceLabel?.removeFromSuperview()
    customizeAnotationView()
    self.titleLabel = makeTitle()
    makeDistanceLabel()
    createView()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    titleLabel?.frame    = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 30)
    distanceLabel?.frame = CGRect(x: 0, y: 27, width: self.frame.size.width, height: 20)
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    delegate?.didTouch(annotationView: self)
  }
}
