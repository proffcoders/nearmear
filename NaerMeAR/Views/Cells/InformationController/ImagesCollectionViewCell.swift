//
//  ImagesCollectionViewCell.swift
//  NaerMeAR
//
//  Created by Roman Runner on 6/1/17.
//  Copyright © 2017 Roman Bigun LLC. All rights reserved.
//

import UIKit

class ImagesCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
  var imageReference:String = "" {
    willSet(oldValue) {
      let path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=\(oldValue)&key=\(apiKey)"
      self.imageReference = path
      imageView.imageFromUrl(urlString: imageReference, spinner: self.activityIndicator, rounded: false)
    }
  }
  
}
