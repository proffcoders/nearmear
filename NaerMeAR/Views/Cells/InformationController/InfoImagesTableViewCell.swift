//
//  ImagesTableViewCell.swift
//  NaerMeAR
//
//  Created by Roman Runner on 6/1/17.
//  Copyright © 2017 Roman Bigun LLC. All rights reserved.
//

import UIKit

class InfoImagesTableViewCell: UITableViewCell {
  
    @IBOutlet weak var collectionView: UICollectionView!
    var place:Place?
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  

}
extension InfoImagesTableViewCell : UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.place!.photoStack.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IMVCell", for: indexPath) as! ImagesCollectionViewCell
    
    cell.imageReference = self.place!.photoStack[indexPath.row]
    
    
    return cell
  }
  
}
extension InfoImagesTableViewCell : UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
    let itemsPerRow:CGFloat = 1
    let hardCodedPadding:CGFloat = 2
    let itemWidth = ((collectionView.bounds.width / itemsPerRow) - hardCodedPadding)+2
    let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)+20
    return CGSize(width: itemWidth, height: itemHeight)
    
  }
  func scrollToNearestVisibleCollectionViewCell() {
    let visibleCenterPositionOfScrollView = Float(collectionView.contentOffset.x + (self.collectionView!.bounds.size.width / 2))
    var closestCellIndex = -1
    var closestDistance: Float = .greatestFiniteMagnitude
    for i in 0..<collectionView.visibleCells.count {
      let cell = collectionView.visibleCells[i]
      let cellWidth = cell.bounds.size.width
      let cellCenter = Float(cell.frame.origin.x + cellWidth / 2)
      
      // Now calculate closest cell
      let distance: Float = fabsf(visibleCenterPositionOfScrollView - cellCenter)
      if distance < closestDistance {
        closestDistance = distance
        closestCellIndex = collectionView.indexPath(for: cell)!.row
      }
    }
    if closestCellIndex != -1 {
      self.collectionView!.scrollToItem(at: IndexPath(row: closestCellIndex, section: 0), at: .centeredHorizontally, animated: true)
    }
  }
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    scrollToNearestVisibleCollectionViewCell()
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if !decelerate {
      scrollToNearestVisibleCollectionViewCell()
    }
  }
  
}
