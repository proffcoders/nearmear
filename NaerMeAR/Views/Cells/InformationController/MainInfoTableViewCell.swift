//
//  MainInfoTableViewCell.swift
//  NaerMeAR
//
//  Created by Roman Runner on 6/1/17.
//  Copyright © 2017 Roman Bigun LLC. All rights reserved.
//

import UIKit

class MainInfoTableViewCell: UITableViewCell {
  
  @IBOutlet weak var timeLbl: UILabel!
  @IBOutlet weak var adressLbl: UILabel!
  @IBOutlet weak var typeLbl: UILabel!
  @IBOutlet weak var distanceLbl: UILabel!
  @IBOutlet weak var phoneLbl: UILabel!
  @IBOutlet weak var websiteLbl: UILabel!
  @IBOutlet weak var ratingLbl: UILabel!
  @IBOutlet weak var workingHoursLbl: UILabel!
  @IBOutlet weak var openedImageView:UIImageView!
  
  var isOpend:Bool = false {
    willSet(newValue) {
      if newValue == false {
        openedImageView.image = UIImage(named: "closed")
      } else if newValue == true {
        openedImageView.image = UIImage(named: "opened")
      }
      
    }
  }
  var distance:Double?
  var timeToTarget = TimeToTarget()
  
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
  func showInfoFor(place: Place) {
    
    print(place.distanceFromUser)
    self.distanceLbl.text              = String(format: "%.2f km", place.distanceFromUser / 1000)
    self.timeToTarget.distanceToTarget = place.distanceFromUser
    self.timeLbl.text                  = timeToTarget.description
    self.websiteLbl.text               = place.website ?? ""
    self.typeLbl.text                  = place.type?.replaceDashesWithWSAndCapitalizeEachWord()
    self.phoneLbl.text                 = place.phoneNumber ?? ""
    self.adressLbl.text                = "\(place.address)"
    self.ratingLbl.text                = "\(place.rating)"
    self.workingHoursLbl.text          = place.workingHoursByDays
    guard let open = place.opennow else { return }
    self.isOpend                       = open
    
  }


}
