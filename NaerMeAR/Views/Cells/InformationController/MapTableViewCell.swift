//
//  MapTableViewCell.swift
//  NaerMeAR
//
//  Created by Roman Runner on 6/2/17.
//  Copyright © 2017 Roman Bigun LLC. All rights reserved.
//

import UIKit
import MapKit



class MapTableViewCell: UITableViewCell,MKMapViewDelegate,CLLocationManagerDelegate {
  
    @IBOutlet weak var mapView: MKMapView!
    var place:Place?
    var userLocation:UserLocation?
    let myPin = MKPointAnnotation()
  
  fileprivate lazy var locationManager:CLLocationManager = {
    
    var _locationManager = CLLocationManager()
    _locationManager.distanceFilter = 10
    _locationManager.activityType = .fitness
    _locationManager.allowsBackgroundLocationUpdates = true
    _locationManager.pausesLocationUpdatesAutomatically = true
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest
    _locationManager.startUpdatingLocation()
    _locationManager.requestWhenInUseAuthorization()
    
    return _locationManager
  }()
  
    override func awakeFromNib() {
        super.awakeFromNib()
      locationManager.delegate = self as! CLLocationManagerDelegate
      locationManager.startUpdatingLocation()
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  func initCellWith(place:Place,userLocation:UserLocation) {
    self.place = place
    self.userLocation = userLocation
    
    queueBackground.async {
    
      self.drawRouting()
      
    }
    
  }
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
    
  }
  func drawRouting() {
  
    let sourceLocation       = CLLocationCoordinate2D(latitude: (place?.location?.coordinate.latitude)!, longitude: (place?.location?.coordinate.longitude)!)
    let destinationLocation  = self.userLocation?.location
    let sourcePlacemark      = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
    let destinationPlacemark = MKPlacemark(coordinate: destinationLocation!, addressDictionary: nil)
    let sourceMapItem        = MKMapItem(placemark: sourcePlacemark)
    let destinationMapItem   = MKMapItem(placemark: destinationPlacemark)
    
    //MARK: Make annotations for source and destination points
    
    let sourceAnnotation             = MKPointAnnotation()
    sourceAnnotation.title           = "Im here"
    if let location                  = sourcePlacemark.location {
      sourceAnnotation.coordinate      = location.coordinate
    }
    let destinationAnnotation        = MKPointAnnotation()
    destinationAnnotation.title      = place?.placeName
    
    if let location                  = destinationPlacemark.location {
      destinationAnnotation.coordinate = location.coordinate
    }
    self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
    
    //MARK: Create direction request
    let directionRequest           = MKDirectionsRequest()
    directionRequest.source        = sourceMapItem
    directionRequest.destination   = destinationMapItem
    directionRequest.transportType = .automobile
    
    //MARK: Calculate the direction
    let directions = MKDirections(request: directionRequest)
    
    directions.calculate {
      (response, error) -> Void in
      
      guard let response = response else {
        if let error = error {
          print("Error: \(error)")
        }
        
        return
      }
      
      let route = response.routes[0]
      self.mapView.add((route.polyline), level: MKOverlayLevel.aboveLabels)
      
      let rect = route.polyline.boundingMapRect
      self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
      
    }
    
    
  }
  //MARK: Draw route
  func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
    let renderer = MKPolylineRenderer(overlay: overlay)
    renderer.strokeColor = UIColor.blue
    renderer.lineWidth = 3.0
    return renderer
  }

  
}
