//
//  ImagesCollectionViewCell.swift
//  NaerMeAR
//
//  Created by Roman Runner on 6/1/17.
//  Copyright © 2017 Roman Bigun LLC. All rights reserved.
//

import UIKit

class ReviewCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var userNameLbl : UILabel!
  @IBOutlet weak var ratingLbl   : UILabel!
  @IBOutlet weak var reviewText  : UITextView!
  @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
  var avatarPath:String = "" {
    willSet(oldValue) {
      self.avatarPath = oldValue
      imageView.imageFromUrl(urlString: self.avatarPath, spinner: self.activityIndicator, rounded: false)
    }
  }
  func initWith(review:Review) {
  
    self.avatarPath = review.authorAvatar
    self.userNameLbl.text = review.authorName
    self.ratingLbl.text = "\(review.userRating)"
    self.reviewText.text = review.message
    
  }
  
  
}
