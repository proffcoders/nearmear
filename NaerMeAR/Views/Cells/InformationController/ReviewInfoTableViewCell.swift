//
//  ReviewInfoTableViewCell.swift
//  NaerMeAR
//
//  Created by Roman Runner on 6/1/17.
//  Copyright © 2017 Roman Bigun LLC. All rights reserved.
//

import UIKit

class ReviewInfoTableViewCell: UITableViewCell {
  
    @IBOutlet weak var reviewLbl:UILabel!
    var reviewsCount:Int!
    var rating:Double!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  func initWith(reviewsCount:Int, rating:Double) {
    self.reviewsCount = reviewsCount
    self.rating = rating
    self.reviewLbl.text = "Reviews: \(self.reviewsCount!) | Overall rating: \(self.rating!)"
    
  }
  
}
