//
//  FilterView.swift
//  NaerMeAR
//
//  Created by Roman Runner on 5/25/17.
//  Copyright © 2017 Roman Bigun LLC. All rights reserved.
//

import UIKit

let kNMUpdateAnnotationsNotification = "kNMUpdateAnnotationsNotification"

class FilterView: UIView,UITableViewDelegate,UITableViewDataSource {
  
     @IBOutlet weak var tableView: UITableView!
     var dataSource:Array<String>!
  
      func initTableView() {
      
        self.tableView = UITableView(frame: frame)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
      
      }
      override init(frame: CGRect) {
        super.init(frame: frame)
        initTableView()
        
      }
  
      required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
        
      }
  
      private func initFilter(completion: (() -> Swift.Void)? = nil) {
        
          var myDict: NSDictionary?
          if let path = Bundle.main.path(forResource: "poisTypes", ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
            if let dict = myDict?.object(forKey: "Types") {
              self.dataSource = dict as! Array<String>
              
            }
          }
        
      }
  
  // MARK: - Table view data source
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if self.dataSource == nil {
        initFilter()
        return self.dataSource.count
    }
    
    return self.dataSource.count
  }
 
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as! FilterTableViewCell
    cell.filterType.text = self.dataSource[indexPath.row].replaceDashesWithWSAndCapitalizeEachWord()
    
    return cell
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    currentFilter = dataSource[indexPath.row]
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNMUpdateAnnotationsNotification), object: nil)
  }

}

