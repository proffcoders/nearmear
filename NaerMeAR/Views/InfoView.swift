//
//  InfoView.swift
//  NearMeAR
//
//  Created by Roman Runner on 5/20/17.
//  Copyright © 2017 Roman Bigun LLC All rights reserved.
//

import UIKit

class InfoView: UIView {

  @IBOutlet weak var reviewLbl: UILabel!
  @IBOutlet weak var timeLbl: UILabel!
  @IBOutlet weak var ratingLbl: UILabel!
  @IBOutlet weak var callButton: UIButton!
  @IBOutlet weak var wwwButton: UIButton!
  @IBOutlet weak var nameLbl: UILabel!
  @IBOutlet weak var infoLbl: UILabel!
  @IBOutlet weak var distanceLbl: UILabel!
  @IBOutlet weak var image: UIImageView!
  @IBOutlet weak var imageType: UIImageView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  var distance:Double?
  var timeToTarget = TimeToTarget()
  func setLayerFor(view:UIView) {
  
    view.layer.cornerRadius = 9
    view.layer.borderWidth  = 2
    view.layer.borderColor  = UIColor.green.cgColor
  
  }
  func showInfoFor(place: Place) {

      if place.phoneNumber == nil {
         self.callButton.isEnabled = false
      } else {
         self.callButton.isEnabled = true
      }
      if place.website == nil {
        self.wwwButton.isEnabled = false
      } else {
        self.wwwButton.isEnabled = true
      }
      print(place.distanceFromUser)
      self.distanceLbl.text              = String(format: "%.2f km", place.distanceFromUser / 1000)
      self.timeToTarget.distanceToTarget = place.distanceFromUser
      self.timeLbl.text                  = timeToTarget.description
    
      setLayerFor(view: self.distanceLbl)
    
      self.nameLbl.text                  = place.placeName
      self.infoLbl.text                  = "\(place.address)\nType: \(place.type!)\n\(place.phoneNumber ?? "")\n\(place.website ?? "")"
      self.ratingLbl.text                = "\(place.rating)"
    
      if place.photoReference.characters.count>0 {
         self.image.imageFromUrl(urlString: place.imageAbsolutePath, spinner: self.activityIndicator)
        
      }
      self.imageType.imageFromUrl(urlString: place.iconPath, spinner: nil, rounded: false)
      self.self.reviewLbl.text = "Reviews:\(place.reviews.count)"
    
    
  }
  func clearData() {
    self.timeLbl.text     = ""
    self.distanceLbl.text = ""
    self.nameLbl.text     = ""
    self.infoLbl.text     = ""
    self.image.image      = UIImage(named: "noimagefound")
  
  }
  
}
