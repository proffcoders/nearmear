
import Foundation
import MapKit

class PlaceAnnotation: NSObject, MKAnnotation {
  let coordinate: CLLocationCoordinate2D
  let title: String?
  let place: Place
  
  init(location: CLLocationCoordinate2D, title: String,placeInfo:Place) {
    self.coordinate = location
    self.title      = title
    self.place      = placeInfo
    
    super.init()
  }
}
